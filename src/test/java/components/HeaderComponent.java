package components;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;
import io.qameta.allure.Step;

public class HeaderComponent extends BasePage {

    private final static String PROFILE_AND_SETTINGS_ICON = "//a[@title='Profile and Settings']";
    private final static String LIST_BUTTON = "//div[@data-role='popup']//a[text()='Lists']";
    private final static String VIEW_PROFILE_BUTTON = "//div[@data-role='popup']//a[text()='View profile']";
    private final static String DISCUSSION_BUTTON = "//div[@data-role='popup']//a[text()='Discussions']";
    private final static String RATINGS_BUTTON = "//div[@data-role='popup']//a[text()='Ratings']";
    private final static String WATCHLIST_BUTTON = "//div[@data-role='popup']//a[text()='Watchlist']";
    private final static String EDIT_PROFILE_BUTTON = "//div[@data-role='popup']//a[text()='Edit Profile']";
    private final static String SETTINGS_BUTTON = "//div[@data-role='popup']//a[text()='Settings']";
    private final static String LOGOUT_BUTTON = "//div[@data-role='popup']//a[text()='Logout']";
    private final static String LOGIN_BUTTON = "//div[@class='flex']//a[text()='Login']";
    private final static String JOIN_TMDB = "//a[text()='Join TMDB']";
    private final static String LOGO = "//a[@class='logo']";

    public SelenideElement getProfileAndSettingsIcon() {
        return getClickableOfElement(PROFILE_AND_SETTINGS_ICON);
    }

    public SelenideElement getViewProfileButton() {
        return getVisibleOfElement(VIEW_PROFILE_BUTTON);
    }

    public SelenideElement getDiscussionButton() {
        return getVisibleOfElement(DISCUSSION_BUTTON);
    }

    public SelenideElement getRatingsButton() {
        return getVisibleOfElement(RATINGS_BUTTON);
    }

    public SelenideElement getWatchlistButton() {
        return getVisibleOfElement(WATCHLIST_BUTTON);
    }

    public SelenideElement getEditProfileButton() {
        return getVisibleOfElement(EDIT_PROFILE_BUTTON);
    }

    public SelenideElement getSettingsButton() {
        return getVisibleOfElement(SETTINGS_BUTTON);
    }

    public SelenideElement getLogoutButton() {
        return getVisibleOfElement(LOGOUT_BUTTON);
    }

    public SelenideElement getLogo() {
        return getVisibleOfElement(LOGO);
    }

    @Step("Click profile ")
    public HeaderComponent clickProfileAndSettingsIcon() {
        scrollPageToElement(getProfileAndSettingsIcon());
        getProfileAndSettingsIcon().click();
        return this;
    }

    public SelenideElement getListButton() {
        return getVisibleOfElement(LIST_BUTTON);
    }

    @Step("Click to list")
    public HeaderComponent clickListButton() {
        getListButton().click();
        return this;
    }

    private SelenideElement getLoginButton() {
        return getVisibleOfElement(LOGIN_BUTTON);
    }

    @Step("TMDB login")
    public HeaderComponent clickLogin() {
        getLoginButton().click();
        return this;
    }

    protected SelenideElement getJoinTMDB() {
        return getVisibleOfElement(JOIN_TMDB);
    }

    @Step("Click join TMDB")
    public HeaderComponent clickJoinTMDB() {
        getJoinTMDB().click();
        return this;
    }

    @Step("Click logo")
    public HeaderComponent clickLogo() {
        getLogo().click();
        return this;
    }

    @Step("Click edit profile")
    public HeaderComponent clickEditProfile() {
        getEditProfileButton().click();
        return this;
    }

    @Step("Click watchlist")
    public HeaderComponent clickWatchListButton() {
        getWatchlistButton().click();
        return this;
    }
}
