package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class OverviewProfileElements extends BasePage {

    private final static String ACCOUNT_NAME_HEADER = "//div[@class='content_wrapper flex']//a";

    public SelenideElement getAccountNameHeader() {
        return getVisibleOfElement(ACCOUNT_NAME_HEADER);
    }
}
