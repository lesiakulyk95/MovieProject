package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class WatchlistElements extends BasePage {
    private final static String YOUR_RATING = "//span[@class='rating wrapper']";
    private final static String RATING_STARS = "//span[@class='filled-stars']/span[1]";
    private final static String FAVORITE = "//span[@class='favourite wrapper']";
    private final static String ADD_TO_LIST = "//span[@class='list wrapper']";
    private final static String REMOVE = "//a[contains(@class,'remove_list')]";
    private final static String OVERVIEW = "//div[@class='overview true']";
    private final static String RATING_RESULT = "//input[@id='media_rating_input_5f15ecc04083b30034079b79']";
    private final static String MOVIE_HEADER = "//h2[text()='Leave the World Behind']";
    private final static String YOU_HAVE_NOT_ADD_MOVIES_TO_YOUR_WATCHLIST_HEADER = "//div[@class='items_wrapper']/p";
    private final static String SUCCESS_NOTIFICATION = "//div[@class='notification success']";
    private final static String ADDED_TO_FAVORITE = "//a[contains(@class,'no_click selected')]";
    private final static String CREATE_LIST_POP_UP = "//div[@class='tooltip_popup']";

    public SelenideElement getYourRatingButton() {
        return getVisibleOfElement(YOUR_RATING);
    }

    protected SelenideElement getRatingStars() {
        return getPresenceOfElement(RATING_STARS);
    }

    protected SelenideElement getFavorite() {
        return getClickableOfElement(FAVORITE);
    }

    protected SelenideElement getAddToList() {
        return getVisibleOfElement(ADD_TO_LIST);
    }

    protected SelenideElement getRemove() {
        return getVisibleOfElement(REMOVE);
    }

    protected SelenideElement getOverview() {
        return getVisibleOfElement(OVERVIEW);
    }

    protected SelenideElement getRatingResult() {
        return getVisibleOfElement(RATING_RESULT);
    }

    public SelenideElement getMovieHeader() {
        return getVisibleOfElement((MOVIE_HEADER));
    }

    public SelenideElement getYouHaveNotAddAnyMoviesToYourWatchlist() {
        return getVisibleOfElement(YOU_HAVE_NOT_ADD_MOVIES_TO_YOUR_WATCHLIST_HEADER);
    }

    public SelenideElement getSuccessNotification() {
        return getVisibleOfElement(SUCCESS_NOTIFICATION);
    }

    public SelenideElement getAddedToFavorite() {
        return getVisibleOfElement(ADDED_TO_FAVORITE);
    }

    public SelenideElement getCreateListPopUp() {
        return getVisibleOfElement(CREATE_LIST_POP_UP);
    }
}
