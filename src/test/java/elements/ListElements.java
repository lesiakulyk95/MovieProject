package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class ListElements extends BasePage {

    private final static String CREATE_LIST_BUTTON = "//a[contains(@class,'button') and text()='Create List']";
    private final static String FIRST_LIST_ITEM = "//a[contains(text(),'%s')]";
    private final static String MOVIE_DESCRIPTION_HEADER = "//div[@class='description']//p[text()='Only for testing']";
    private final static String EDIT_BUTTON = "//a[text()='Edit']";

    protected SelenideElement getCreateListButton() {
        return getVisibleOfElement(CREATE_LIST_BUTTON);
    }

    protected SelenideElement getFirstListItem(String listName) {
        return getVisibleOfElement(String.format(FIRST_LIST_ITEM, listName));
    }

    public SelenideElement getMovieDescription() {
        return getVisibleOfElement(MOVIE_DESCRIPTION_HEADER);
    }

    protected SelenideElement getEditButton() {
        return getVisibleOfElement(EDIT_BUTTON);
    }
}
