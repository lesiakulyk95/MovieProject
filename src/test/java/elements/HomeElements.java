package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class HomeElements extends BasePage {

    private final static String ACCEPT_ALL_COOKIES = "//button[@id='onetrust-accept-btn-handler']";
    private final static String SEARCH_FIELD_INPUT = "//input[@id='inner_search_v4']";
    private final static String SEARCH_BUTTON = "//input[@value='Search']";
    private final static String FIRST_MOVIE_OF_TRENDING = "//div[@id='trending_scroller']//div[@class='card style_1'][1]";

    protected SelenideElement getAcceptAllCookiesButton() {
        return getClickableOfElement(ACCEPT_ALL_COOKIES);
    }

    protected SelenideElement getSearchFieldInput() {
        return getVisibleOfElement(SEARCH_FIELD_INPUT);
    }

    protected SelenideElement getSearchButton() {
        return getVisibleOfElement(SEARCH_BUTTON);
    }

    protected SelenideElement getMovieOfTrending() {
        return getVisibleOfElement(FIRST_MOVIE_OF_TRENDING);
    }
}
