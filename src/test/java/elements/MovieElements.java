package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class MovieElements extends BasePage {
    private final static String WATCHLIST_BUTTON = "//a[@id='watchlist']";

    private final static String INFORMATION_ABOUT_MOVIE = "//div[@class='content_wrapper']";


    protected SelenideElement getWatchlistButton() {
        return getVisibleOfElement(WATCHLIST_BUTTON);
    }

    public SelenideElement getInformationAboutMovie() {
        return getVisibleOfElement(INFORMATION_ABOUT_MOVIE);
    }
}
