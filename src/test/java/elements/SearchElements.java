package elements;

import com.codeborne.selenide.ElementsCollection;
import common.BasePage;

public class SearchElements extends BasePage {
    private final static String LIST_OF_SEARCH_RESULT = "//a[@data-media-type='movie']//h2";

    protected ElementsCollection getListOfSearchResult() {
        return getListPresenceOfElements(LIST_OF_SEARCH_RESULT);
    }
}
