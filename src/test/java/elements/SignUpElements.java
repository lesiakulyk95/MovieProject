package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class SignUpElements extends BasePage {

    private final static String USERNAME_INPUT = "//input[@id='username']";
    private final static String PASSWORD_INPUT = "//input[@id='password']";
    private final static String PASSWORD_CONFIRM_INPUT = "//input[@id='password_confirm']";
    private final static String EMAIL_INPUT = "//input[@id='email']";
    private final static String SIGN_UP_BUTTON = "//input[@id='sign_up_button']";
    private final static String ERROR_SIGN_UP_MASSAGE = "//h2[@class='background_color red']";
    private final static String ERROR_MASSAGE_NAME = "//li[text()='Username has already been taken']";
    private final static String ERROR_MASSAGE_PASSWORD = "//li[text()='Password and password confirmation do not match']";
    private final static String ERROR_MASSAGE_EMAIL = "//li[text()='Email does not appear to be a valid email address']";

    protected SelenideElement getUserNameInput() {
        return getVisibleOfElement(USERNAME_INPUT);
    }

    protected SelenideElement getPasswordInput() {
        return getVisibleOfElement(PASSWORD_INPUT);
    }

    protected SelenideElement getPasswordConfirmInput() {
        return getVisibleOfElement(PASSWORD_CONFIRM_INPUT);
    }

    protected SelenideElement getEmailInput() {
        return getVisibleOfElement(EMAIL_INPUT);
    }

    protected SelenideElement getSignUpButton() {
        return getVisibleOfElement(SIGN_UP_BUTTON);
    }

    public SelenideElement getErrorSignUpMassage() {
        return getVisibleOfElement(ERROR_SIGN_UP_MASSAGE);
    }

    public SelenideElement getErrorAlreadyTakenNameMassage() {
        return getVisibleOfElement(ERROR_MASSAGE_NAME);
    }

    public SelenideElement getErrorInvalidPasswordMassage() {
        return getVisibleOfElement(ERROR_MASSAGE_PASSWORD);
    }

    public SelenideElement getErrorInvalidEmailMassage() {
        return getVisibleOfElement(ERROR_MASSAGE_EMAIL);
    }


}
