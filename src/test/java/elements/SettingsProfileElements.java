package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class SettingsProfileElements extends BasePage {

    private final static String GENDER_DROPDOWN = "//span[@class='k-select']";
    private final static String MALE_GENDER = "//li[text()='Male']";
    private final static String NAME_INPUT = "//input[@id='name']";
    private final static String UPLOAD_YOUR_OWN_BUTTON = "//a[contains(@class,'upload_avatar')]";
    private final static String SELECT_FILE_BUTTON = "//input[@id='upload_files']";
    private final static String SAVE_BUTTON = "//input[@value='Save']";
    private final static String SUCCESSFUL_CHANGED_MESSAGE = "//div[@class='notification success']";
    private final static String ACCOUNT_HEADER = "//a//img[@class='avatar']";
    private final static String REMOVE_YOUR_AVATAR_BUTTON = "//a[contains(@class,'remove_avatar')]";
    private final static String EMPTY_GENDER = "//li[text()='-']";
    private final static String YES_BUTTON = "//button[text()='Yes']";


    protected SelenideElement getGenderDropdown() {
        return getPresenceOfElement(GENDER_DROPDOWN);
    }

    protected SelenideElement getMaleGender() {
        return getVisibleOfElement(MALE_GENDER);
    }

    protected SelenideElement getEmptyGender() {
        return getVisibleOfElement(EMPTY_GENDER);
    }

    protected SelenideElement getNameInput() {
        return getVisibleOfElement(NAME_INPUT);
    }

    protected SelenideElement getUploadYourOwnButton() {
        return getVisibleOfElement(UPLOAD_YOUR_OWN_BUTTON);
    }

    protected SelenideElement getSelectFileButton() {
        return getPresenceOfElement(SELECT_FILE_BUTTON);
    }

    protected SelenideElement getSaveButton() {
        return getVisibleOfElement(SAVE_BUTTON);
    }

    public SelenideElement getSuccessfulChangedMessage() {
        return getVisibleOfElement(SUCCESSFUL_CHANGED_MESSAGE);
    }

    protected SelenideElement getAccountHeader() {
        return getVisibleOfElement(ACCOUNT_HEADER);
    }

    protected SelenideElement getRemoveYourAvatarButton() {
        return getVisibleOfElement(REMOVE_YOUR_AVATAR_BUTTON);
    }

    protected SelenideElement getConfirmAvatarDeleting() {
        return getVisibleOfElement(YES_BUTTON);
    }
}
