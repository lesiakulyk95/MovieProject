package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class LoginElements extends BasePage {

    private final static String NAME_INPUT = "//input[@id='username']";
    private final static String PASSWORD_INPUT = "//input[@id='password']";
    private final static String LOGIN_BUTTON = "//input[@id='login_button']";

    protected SelenideElement getNameInput() {
        return getVisibleOfElement(NAME_INPUT);
    }

    protected SelenideElement getPasswordInput() {
        return getVisibleOfElement(PASSWORD_INPUT);
    }

    protected SelenideElement getLoginButton() {
        return getVisibleOfElement(LOGIN_BUTTON);
    }
}
