package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class EditListElements extends BasePage {

    private final static String ADD_ITEM_INPUT = "//input[@id='list_item_search']";
    private final static String MOVIE_DESCRIPTION_TEXTAREA = "//textarea[@class='k-textbox']";
    private final static String SAVE_BUTTON = "//input[@value='Save']";
    private final static String CHOOSE_IMAGE = "//a[text()='Choose Image']";
    private final static String SELECT_THIS_IMAGE = "//p[text()='Select this image']";
    private final static String SELECTED_IMAGE = "//p[@class='selected']";
    private final static String FIRST_SUGGESTED_ITEM = "//li[@data-offset-index='0']";
    private final static String MOVIE_PICTURE = "//img[@class='backdrop']";
    private final static String DELETE_LIST_STEP = "//a[text()='Delete List']";
    private final static String CONFIRM_DELETE_LIST = "//button[text()='Yes']";
    private final static String DELETE_LIST_BUTTON = "//button[@id='delete_list']";

    protected SelenideElement getAddItemInput() {
        return getVisibleOfElement(ADD_ITEM_INPUT);
    }

    protected SelenideElement getMovieDescriptionTextarea() {
        return getVisibleOfElement(MOVIE_DESCRIPTION_TEXTAREA);
    }

    protected SelenideElement getSaveButton() {
        return getVisibleOfElement(SAVE_BUTTON);
    }

    protected SelenideElement getChooseImage() {
        return getVisibleOfElement(CHOOSE_IMAGE);
    }

    protected SelenideElement getSelectThisImage() {
        return getVisibleOfElement(SELECT_THIS_IMAGE);
    }

    public SelenideElement getSelectedImage() {
        return getVisibleOfElement(SELECTED_IMAGE);
    }

    protected SelenideElement getFirstSuggestedItem() {
        return getVisibleOfElement(FIRST_SUGGESTED_ITEM);
    }

    protected SelenideElement getMoviePicture() {
        return getVisibleOfElement(MOVIE_PICTURE);
    }

    protected SelenideElement getDeleteListStep() {
        return getVisibleOfElement(DELETE_LIST_STEP);
    }

    protected SelenideElement getDeleteListButton() {
        return getVisibleOfElement(DELETE_LIST_BUTTON);
    }

    protected SelenideElement getConfirmDeleteList() {
        return getVisibleOfElement(CONFIRM_DELETE_LIST);
    }
}
