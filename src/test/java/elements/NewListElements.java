package elements;

import com.codeborne.selenide.SelenideElement;
import common.BasePage;

public class NewListElements extends BasePage {

    private final static String NAME_INPUT = "//input[@id='name']";
    private final static String DESCRIPTION_INPUT = "//textarea[@id='description']";
    private final static String PUBLIC_LIST_DROPDOWN = "//span[contains(@aria-owns,'public_listbox')]";
    private final static String NO_PUBLIC_SELECTOR = "//li[text()='No']";
    private final static String SORT_BY_DROPDOWN = "//span[contains(@aria-owns,'sort_by_listbox')]";
    private final static String CONTINUE_BUTTON = "//input[@value='Continue']";

    public SelenideElement getNameInput() {
        return getVisibleOfElement(NAME_INPUT);
    }

    public SelenideElement getDescriptionInput() {
        return getVisibleOfElement(DESCRIPTION_INPUT);
    }

    public SelenideElement getPublicListDropdown() {
        return getVisibleOfElement(PUBLIC_LIST_DROPDOWN);
    }

    public SelenideElement getNoPublicSelector() {
        return getVisibleOfElement(NO_PUBLIC_SELECTOR);
    }

    public SelenideElement getSortByDropdown() {
        return getVisibleOfElement(SORT_BY_DROPDOWN);
    }

    public SelenideElement getContinueButton() {
        return getVisibleOfElement(CONTINUE_BUTTON);
    }
}
