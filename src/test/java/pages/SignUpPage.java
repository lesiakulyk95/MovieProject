package pages;

import elements.SignUpElements;
import io.qameta.allure.Step;

import static java.lang.Thread.sleep;

public class SignUpPage extends SignUpElements {

    @Step("Set invalid username")
    public SignUpPage inputInvalidUserName(String username) {
        getUserNameInput().sendKeys(username);
        return this;
    }

    @Step("Set invalid password")
    public SignUpPage inputInvalidPassword(String password) {
        getPasswordInput().sendKeys(password);
        return this;
    }

    @Step("Set confirm invalid password")
    public SignUpPage inputConfirmInvalidPassword(String confirmPassword) {
        getPasswordConfirmInput().sendKeys(confirmPassword);
        return this;
    }

    @Step("Set email")
    public SignUpPage inputEmail(String email) {
        getEmailInput().sendKeys(email);
        return this;
    }

    @Step("Click signup button")
    public SignUpPage clickSignUpButton() throws InterruptedException {
        sleep(1000);
        getSignUpButton().click();
        return this;
    }
}
