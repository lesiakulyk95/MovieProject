package pages;

import elements.EditListElements;
import io.qameta.allure.Step;

public class EditListPage extends EditListElements {

    @Step("Set item name")
    public EditListPage inputAddItem(String movie) {
        getAddItemInput().sendKeys(movie);
        getFirstSuggestedItem().click();
        return this;
    }

    @Step("Input movie description")
    public EditListPage inputMovieDescription(String text) {
        getMovieDescriptionTextarea().sendKeys(text);
        return this;
    }

    @Step("Click choose image step")
    public EditListPage clickChooseImage() {
        getChooseImage().click();
        return this;
    }

    @Step("Click select this image")
    public EditListPage clickSelectThisImage() {
        getMoviePicture().click();
        getSelectThisImage().click();
        return this;
    }

    @Step("Click save")
    public EditListPage clickSaveButton() {
        getSaveButton().click();
        return this;
    }

    @Step("Click delete list step")
    public EditListPage clickDeleteListStep() {
        getDeleteListStep().click();
        return this;
    }

    @Step("Click delete list")
    public EditListPage clickDeleteListButton() {
        getDeleteListButton().click();
        return this;
    }

    @Step("Click confirm delete")
    public EditListPage clickConfirmDeleteList() {
        getConfirmDeleteList().click();
        return this;
    }

}
