package pages;

import elements.MovieElements;
import io.qameta.allure.Step;

public class MoviePage extends MovieElements {

    @Step("Click add to watchlist")
    public MoviePage clickToWatchlistButton() {
        getWatchlistButton().click();
        return this;

    }
}
