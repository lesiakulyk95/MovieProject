package pages;

import elements.WatchlistElements;
import io.qameta.allure.Step;

public class MyWatchlistPage extends WatchlistElements {

    @Step("Click you rating")
    public MyWatchlistPage clickYourRatingButton() {
        getYourRatingButton().click();
        return this;
    }

    @Step("Estimate movie")
    public MyWatchlistPage clickRatingStars() {
        moveToElement(getRatingStars());
        getRatingStars().click();
        return this;
    }

    @Step("Click favourite")
    public MyWatchlistPage clickFavorite() {
        getFavorite().click();
        return this;
    }

    @Step("Click add to list")
    public MyWatchlistPage clickAddToList() {
        getAddToList().click();
        return this;
    }

    @Step("Click remove")
    public MyWatchlistPage clickRemove() {
        getRemove().click();
        return this;
    }

    @Step("Click overview")
    public MyWatchlistPage clickOverview() {
        getOverview().click();
        return this;
    }
}
