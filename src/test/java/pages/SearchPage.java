package pages;

import elements.SearchElements;
import io.qameta.allure.Step;

public class SearchPage extends SearchElements {

    @Step("Verify search result")
    public boolean isListOfSearchResultVisibleByName(String text) {
        return getListOfSearchResult().texts().contains(text);
    }
}
