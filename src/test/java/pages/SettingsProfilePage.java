package pages;

import elements.SettingsProfileElements;
import io.qameta.allure.Step;

import java.io.File;

public class SettingsProfilePage extends SettingsProfileElements {

    @Step("Select male gender")
    public SettingsProfilePage selectMaleGender() {
        getGenderDropdown().click();
        getMaleGender().click();
        return this;
    }

    @Step("Input name")
    public SettingsProfilePage inputName(String name) {
        getNameInput().sendKeys(name);
        return this;
    }

    @Step("Click upload your own button")
    public SettingsProfilePage clickUploadYourOwnButton() {
        getUploadYourOwnButton().click();
        return this;
    }

    @Step("Click select file")
    public SettingsProfilePage clickSelectFile() {
        getSelectFileButton().uploadFile(new File("src/test/resources/uploadedPhotos/photo_2023-09-21_17-57-46.jpg"));
        return this;
    }

    @Step("Click save")
    public SettingsProfilePage clickSaveButton() {
        moveToElement(getSaveButton());
        getSaveButton().click();
        return this;
    }

    @Step("Click account header")
    public SettingsProfilePage clickAccountHeader() {
        moveToElement(getAccountHeader());
        getAccountHeader().click();
        return this;
    }

    @Step("Click remove your avatar")
    public SettingsProfilePage clickRemoveYourAvatar() {
        getRemoveYourAvatarButton().click();
        getConfirmAvatarDeleting().click();
        return this;
    }

    @Step("Select empty gender")
    public SettingsProfilePage selectEmptyGender() {
        scrollPageToElement(getGenderDropdown());
        getGenderDropdown().click();
        getEmptyGender().click();
        return this;
    }

    @Step("Clear current name")
    public SettingsProfilePage clearNameInput() {
        getNameInput().clear();
        return this;
    }
}
