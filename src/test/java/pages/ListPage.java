package pages;

import elements.ListElements;
import io.qameta.allure.Step;

public class ListPage extends ListElements {

    @Step("Click create list button")
    public ListPage clickCreateListButton() {
        getCreateListButton().click();
        return this;
    }

    @Step("Click first suggested item")
    public ListPage clickFirstListItem() {
        getFirstListItem("My test list").click();
        return this;
    }

    @Step("Click edit button")
    public ListPage clickEditButton() {
        getEditButton().click();
        return this;
    }
}
