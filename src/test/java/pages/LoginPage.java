package pages;

import elements.LoginElements;
import io.qameta.allure.Step;

import static utils.UserData.USER_NAME;
import static utils.UserData.USER_PASSWORD;

public class LoginPage extends LoginElements {

    @Step("Set user name")
    public LoginPage inputUserName() {
        getNameInput().sendKeys(USER_NAME);
        return this;
    }

    @Step("Set password")
    public LoginPage inputPassword() {
        getPasswordInput().sendKeys(USER_PASSWORD);
        return this;
    }

    @Step("Click login")
    public LoginPage clickLoginButton() {
        getLoginButton().click();
        return this;
    }

    @Step("Login")
    public LoginPage TMDBLogin() {
        inputUserName()
                .inputPassword()
                .clickLoginButton();
        return this;
    }
}
