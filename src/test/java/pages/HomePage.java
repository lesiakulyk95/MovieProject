package pages;

import elements.HomeElements;
import io.qameta.allure.Step;

public class HomePage extends HomeElements {

    @Step("Accept cookies")
    public HomePage clickAcceptAllCookiesButton() {
        getAcceptAllCookiesButton().click();
        return this;
    }

    @Step("Input movie name")
    public HomePage inputSearchField(String text) {
        getSearchFieldInput().click();
        getSearchFieldInput().sendKeys(text);
        return this;
    }

    @Step("Click search")
    public HomePage clickSearchButton() {
        getSearchButton().click();
        return this;
    }

    @Step("Click first movie on trending")
    public HomePage clickMovieOfTrending() {
        getMovieOfTrending().click();
        return this;
    }
}
