package pages;

import elements.NewListElements;
import io.qameta.allure.Step;

public class NewListPage extends NewListElements {

    @Step("Set list name")
    public NewListPage inputName(String name) {
        getNameInput().sendKeys(name);
        return this;
    }

    @Step("Set movie description")
    public NewListPage inputDescription(String text) {
        getDescriptionInput().sendKeys(text);
        return this;
    }

    @Step("Select option no public list")
    public NewListPage selectNoPublicList() {
        getPublicListDropdown().click();
        getNoPublicSelector().click();
        return this;
    }

    @Step("Click continue")
    public NewListPage clickContinueButton() {
        getContinueButton().click();
        return this;
    }
}
