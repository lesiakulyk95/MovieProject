package tests;

import components.HeaderComponent;
import elements.OverviewProfileElements;
import io.qameta.allure.Description;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.SettingsProfilePage;

public class UserEditProfileTest extends TestInit {

    @Test
    @Description("PQ-29: User edit his account")
    public void userEditProfile() {

        HomePage homePage = new HomePage();
        HeaderComponent headerComponent = new HeaderComponent();
        LoginPage loginPage = new LoginPage();
        SettingsProfilePage settingsProfilePage = new SettingsProfilePage();
        OverviewProfileElements overviewProfileElements = new OverviewProfileElements();

        homePage
                .clickAcceptAllCookiesButton();

        headerComponent
                .clickLogin();

        loginPage
                .TMDBLogin();
        headerComponent
                .clickProfileAndSettingsIcon()
                .clickEditProfile();

        settingsProfilePage
                .clickUploadYourOwnButton()
                .clickSelectFile()
                .selectMaleGender()
                .inputName("Oleksandr")
                .clickSaveButton();

        assertTrue(settingsProfilePage.getSuccessfulChangedMessage().isDisplayed());

        settingsProfilePage.clickAccountHeader();

        assertTrue(overviewProfileElements.getAccountNameHeader().getText().contains("Oleksandr"));

        headerComponent
                .clickProfileAndSettingsIcon()
                .clickEditProfile();

        settingsProfilePage
                .clickRemoveYourAvatar()
                .selectEmptyGender()
                .clearNameInput()
                .clickSaveButton();

        headerComponent
                .clickLogo();
    }
}
