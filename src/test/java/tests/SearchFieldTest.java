package tests;

import jdk.jfr.Description;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SearchPage;

public class SearchFieldTest extends TestInit {
    @Test
    @Description("PQ-25: Check the search field.")
    public void searchByNameMovie() {

        HomePage homePage = new HomePage();
        SearchPage searchPage = new SearchPage();

        homePage
                .inputSearchField("Barbie")
                .clickSearchButton();

        assertTrue(searchPage.isListOfSearchResultVisibleByName("Barbie"));

    }
}
