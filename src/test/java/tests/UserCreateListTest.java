package tests;

import components.HeaderComponent;
import io.qameta.allure.Description;
import org.testng.annotations.Test;
import pages.*;

import static com.codeborne.selenide.Selenide.sleep;

public class UserCreateListTest extends TestInit {

    @Test
    @Description("PQ-23 : User create movie list")
    public void userCreateMovieList() {

        HomePage homePage = new HomePage();
        HeaderComponent headerComponent = new HeaderComponent();
        LoginPage loginPage = new LoginPage();
        ListPage listPage = new ListPage();
        NewListPage newListPage = new NewListPage();
        EditListPage editListPage = new EditListPage();

        homePage
                .clickAcceptAllCookiesButton();

        headerComponent
                .clickLogin();

        loginPage
                .TMDBLogin();

        headerComponent
                .clickProfileAndSettingsIcon();

        assertTrue(headerComponent.getViewProfileButton().isDisplayed());
        assertTrue(headerComponent.getDiscussionButton().isDisplayed());
        assertTrue(headerComponent.getListButton().isDisplayed());
        assertTrue(headerComponent.getRatingsButton().isDisplayed());
        assertTrue(headerComponent.getWatchlistButton().isDisplayed());
        assertTrue(headerComponent.getEditProfileButton().isDisplayed());
        assertTrue(headerComponent.getSettingsButton().isDisplayed());
        assertTrue(headerComponent.getLogoutButton().isDisplayed());


        headerComponent.clickListButton();

        assertTrue(getUrl("/u/sanek2070452/lists"));

        listPage
                .clickCreateListButton();

        assertTrue(getUrl("/list/new"));
        assertTrue(newListPage.getNameInput().isDisplayed());
        assertTrue(newListPage.getDescriptionInput().isDisplayed());
        assertTrue(newListPage.getPublicListDropdown().isDisplayed());
        assertTrue(newListPage.getSortByDropdown().isDisplayed());

        newListPage
                .inputName("My test list")
                .inputDescription("Only for testing")
                .selectNoPublicList()
                .clickContinueButton();

        editListPage
                .inputAddItem("Barbie")
                .inputMovieDescription("Only for testing")
                .clickSaveButton()
                .clickChooseImage()
                .clickSelectThisImage();

        assertTrue(editListPage.getSelectedImage().isDisplayed());

        sleep(3000);
        headerComponent
                .clickProfileAndSettingsIcon()
                .clickListButton();

        listPage
                .clickFirstListItem();

        assertTrue(listPage.getMovieDescription().getText().contains("Only for testing"));

        listPage
                .clickEditButton();

        editListPage
                .clickDeleteListStep()
                .clickDeleteListButton()
                .clickConfirmDeleteList();

    }
}
