package tests;

import components.HeaderComponent;
import jdk.jfr.Description;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SignUpPage;

import static utils.UserData.*;

public class InvalidSignUp extends TestInit {

    @Test
    @Description("PQ-22: Create a new account with invalid data.")
    public void userSignUpWithInvalidData() throws InterruptedException {

        HeaderComponent headerComponent = new HeaderComponent();
        HomePage homePage = new HomePage();
        SignUpPage signUpPage = new SignUpPage();

        homePage
                .clickAcceptAllCookiesButton();

        headerComponent
                .clickJoinTMDB();

        signUpPage
                .inputInvalidUserName(INVALID_USERNAME)
                .inputInvalidPassword(INVALID_PASSWORD)
                .inputConfirmInvalidPassword(INVALID_CONFIRM_PASSWORD)
                .inputEmail(INVALID_EMAIL)
                .clickSignUpButton();

        assertTrue(signUpPage.getErrorSignUpMassage().isDisplayed());
        assertTrue(signUpPage.getErrorSignUpMassage().getText().contains("There was an error processing your signup"));
        assertTrue(signUpPage.getErrorAlreadyTakenNameMassage().getText().contains("Username has already been taken"));
        assertTrue(signUpPage.getErrorInvalidPasswordMassage().getText().contains("Password and password confirmation do not match"));
        assertTrue(signUpPage.getErrorInvalidEmailMassage().getText().contains("Email does not appear to be a valid email address"));

    }
}
