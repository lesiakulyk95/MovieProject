package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Allure;
import listeners.TestListeners;
import lombok.SneakyThrows;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import utils.SuiteConfiguration;

import java.io.ByteArrayInputStream;

import static com.codeborne.selenide.Selenide.*;

@Listeners(TestListeners.class)
public class TestInit extends Assert {

    protected static String environment;
    protected static boolean headless;
    protected static String browser;

    @BeforeSuite(alwaysRun = true)
    @SneakyThrows
    public void initTestSuite() {
        SuiteConfiguration config = new SuiteConfiguration();
        environment = config.getProperty("site.url");
        headless = Boolean.parseBoolean(config.getProperty("headless"));
        browser = config.getProperty("browser.mode");
    }

    @BeforeMethod
    public void setup() {
        Configuration.browser = browser;
        Configuration.headless = headless;

        if ((headless)) {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36");

            WebDriver driver = new ChromeDriver(chromeOptions);
            WebDriverRunner.setWebDriver(driver);
        }

        open(environment);

        if (!(headless)) {

            {
                WebDriverRunner.getWebDriver().manage().window().maximize();
            }
        }
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    @AfterMethod
    public void closeTest(ITestResult result) {
        if (result.getStatus() == ITestResult.SKIP || result.getStatus() == ITestResult.FAILURE) {
            TakesScreenshot screenshotDriver = (TakesScreenshot) WebDriverRunner.getWebDriver();
            byte[] screenshot = screenshotDriver.getScreenshotAs(OutputType.BYTES);
            Allure.addAttachment("Test" + result.getMethod().getMethodName(), "image/png",
                    new ByteArrayInputStream(screenshot), "png");
        }
        closeWindow();
    }

    public static boolean getUrl(String endpoint) {
        return WebDriverRunner.getWebDriver().getCurrentUrl().contains(endpoint);
    }
}
