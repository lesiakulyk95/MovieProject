package tests;

import components.HeaderComponent;
import io.qameta.allure.Description;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;
import pages.MoviePage;
import pages.MyWatchlistPage;

import static com.codeborne.selenide.Selenide.refresh;

public class CheckMyWatchListTest extends TestInit {

    @Test
    @Description("PQ-26: Check My Watchlist.")
    public void checkMyWatchlist() {

        HeaderComponent headerComponent = new HeaderComponent();
        HomePage homePage = new HomePage();
        MoviePage moviePage = new MoviePage();
        MyWatchlistPage myWatchlistPage = new MyWatchlistPage();
        LoginPage loginPage = new LoginPage();

        homePage
                .clickAcceptAllCookiesButton();

        headerComponent
                .clickLogin();

        loginPage
                .TMDBLogin();

        headerComponent
                .clickLogo();

        homePage
                .clickMovieOfTrending();

        assertTrue(getUrl("/movie"));

        moviePage
                .clickToWatchlistButton();

        headerComponent
                .clickProfileAndSettingsIcon()
                .clickWatchListButton();

        assertTrue(myWatchlistPage.getMovieHeader().getText().contains("Leave the World Behind"));

        myWatchlistPage
                .clickYourRatingButton();

        assertTrue(myWatchlistPage.getYourRatingButton().isDisplayed());

        myWatchlistPage.clickRatingStars();

        assertTrue(myWatchlistPage.getSuccessNotification().isDisplayed());

        myWatchlistPage
                .clickOverview()
                .clickFavorite();

        assertTrue(myWatchlistPage.getAddedToFavorite().isDisplayed());

        myWatchlistPage
                .clickOverview()
                .clickAddToList();

        assertTrue(myWatchlistPage.getCreateListPopUp().isDisplayed());

        myWatchlistPage
                .clickFavorite()
                .clickRemove();

        refresh();

        assertTrue(myWatchlistPage.getYouHaveNotAddAnyMoviesToYourWatchlist()
                .getText().contains("You haven't added any movies to your watchlist."));

    }
}
